@extends('layouts.app')

@section('content')

    <div class="container">
        <br><br>
        <div class="row">
            <div class="col s12">
                <form class="col s8 offset-s2" method="post" action="{{route('modify', ['id'=>$post->id])}}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    @if(session('success'))
                        <div class="card-panel green darken-1">{{ session('success') }}</div>
                    @endif

                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="postText" name="postText" class="materialize-textarea" data-length="500">{{ $post->postText }}</textarea>
                            <label for="postText">Post text</label>

                            @if($errors->has('postText'))
                                <div class="card-panel red darken-1">{{ $errors->first('postText') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <table>
                                <tbody>
                                    @each('layouts.pictureEdit', $post->images, 'image')
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="file-field input-field">
                        <div class="btn waves-effect waves-light teal lighten-2 grey-text text-darken-4">
                            <span>File</span>
                            <input type="file" name="photos[]" accept="image/gif, image/jpeg, image/jpg, image/png" multiple>
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>

                        @if($errors->has('photos'))
                            <div class="card-panel red darken-1">{{ $errors->first('photos') }}</div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col s12">
                            <div class="center">
                                <button class="btn waves-effect waves-light teal lighten-2 grey-text text-darken-4" type="submit" name="action">Modify post</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection