<div class="col s8 offset-s2">
    <div class="card">
        <div class="card-image">

            <div class="slider">
                <ul class="slides">

                    @each('layouts.picture', $post->images, 'image')

                </ul>
            </div>

        </div>
        <div class="card-content">
            <p>{{$post->postText}}</p>
            <hr>
            <p class="left-align">
                Crée le : <?php $create = explode(' ', $post->created_at); echo $create[0]; ?> | Modifié le : <?php $update = explode(' ', $post->updated_at); echo $update[0] ?>
            </p>
        </div>
        <div class="card-action center-align">
            <a href="{{route('modify', ['id'=>$post->id])}}" class="edit"></a>
            <a href="#modal{{$post->id}}" class="delete modal-trigger"></a>
        </div>
    </div>
</div>

<div id="modal{{ $post->id }}" class="modal">
    <div class="modal-content">
        <h4>Confirmation de suppression</h4>
        <p>Etes vous sur de vouloir supprimer ce post ?</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="modal-action modal-close waves-effect waves-teal btn-flat">Annuler</a>
        <a href="{{route('delete', ['id'=>$post->id])}}" class="modal-action modal-close waves-effect waves-teal btn-flat">Supprimer</a>
    </div>
</div>