<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Agra</title>

    <!-- Styles -->
    <link href="{{ asset('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen">
    <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet" media="screen">

    <!-- Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="white">
        <div class="nav-wrapper container">
            <a href="/" class="logo"></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="/">Accueil</a></li>
                <li><a href="{{ route('upload') }}">Ajouter un post</a></li>
            </ul>

            <ul id="nav-mobile" class="side-nav">
                <li><a href="/">Accueil</a></li>
                <li><a href="{{ route('upload') }}">Ajouter un post</a></li>
            </ul>
            <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>
    </nav>

    <div id="index-banner">
        <img src="{{ asset("/img/header.png") }}" alt="Image" />
    </div>

    @yield('content')

</div>

<footer class="page-footer teal lighten-2">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="grey-text text-darken-4">Company Bio</h5>
                <p class="grey-text text-darken-4">We are a professionnal school of computer science.</p>


            </div>
            <div class="col l3 s12">
                <h5 class="grey-text text-darken-4">Contact</h5>
                <p class="grey-text text-darken-4">Chemin Gérard-de-Ternier 10 </br> 1213 Lancy</p>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container grey-text text-darken-4">
            Made with <a class="grey-text text-darken-3" href="http://materializecss.com">Materialize</a>
        </div>
    </div>
</footer>

<!-- Scripts -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/materialize.js') }}"></script>
<script src="{{ asset('js/init.js') }}"></script>

@yield('script')

</body>
</html>
