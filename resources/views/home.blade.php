@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="section">

            <!--   Icon Section   -->
            <div class="row">

                @if(session('success'))
                    <div class="card-panel green darken-1">{{ session('success') }}</div>
                @endif


            @if(count($posts) == 0)

                    <div class="col s8 offset-s2">
                        <div class="card">
                            <div class="card-image">
                                <img src=" {{ asset('img/coming-soon.jpg') }} ">
                            </div>
                            <div class="card-content red darken-1">
                                <p>
                                    Il n'y a encore aucun post sur ce site ! C'est a venir.
                                </p>
                            </div>
                        </div>
                    </div>

                @else

                    @each('layouts.post', $posts, 'post')

                @endif

            </div>

        </div>
    </div>

@endsection

@section('script')

    <script>
        $(document).ready(function(){
            $('.modal').modal();
            $('.slider').slider();
        });
    </script>

@endsection
