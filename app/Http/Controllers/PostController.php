<?php

namespace App\Http\Controllers;

use App\Image;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function showAll()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();

        return view('home', ['posts' => $posts]);
    }

    public function editForm ($id)
    {
        $post = Post::findOrFail($id);
        return view('article.modifyForm', ['post' => $post]);
    }

    public function editSubmit (Request $request, $id)
    {
        $this->validate($request, [
            'postText' => 'required',
            'photos' => 'max:71680',
            'photos.*' => 'image|mimes:jpeg,jpg,gif,png|max:3072'
        ]);

        $data = Input::all();
        $post = Post::findOrFail($id);

        if($data['postText'] != $post->postText)
        {
            $post->postText = $data['postText'];
            $post->save();
        }

        if(array_key_exists('editPicture', $data) != false)
        {
            foreach($data['editPicture'] as $idImage) {
                $image = Image::findOrFail($idImage);
                Storage::disk('public')->delete($image->nameImage);
                $image->delete();
            }
        }

        if (array_key_exists('photos', $data) != false)
        {
            foreach($data['photos'] as $photo) {
                $filename = $photo->store(config('image.path'), 'public');

                $post->images()->save(
                    new Image([
                        'nameImage'=>$filename
                    ])
                );

                \Intervention\Image\Facades\Image::make($photo->getRealPath())->resize(655, null, function ($constraint) {$constraint->aspectRatio();})->save(public_path('/').$filename);
            }
        }

        return redirect('/')->with('success', 'Article modifié avec succès !');
    }

    public function deleteSubmit($id)
    {
        $post = Post::findOrFail($id);

        foreach ($post->images as $image)
        {
            Storage::disk('public')->delete($image->nameImage);
            $image->delete();
        }

        $post->delete();
        return redirect('/')->with('success', 'Article supprimé avec succès !');
    }
}
