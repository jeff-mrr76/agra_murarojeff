<?php

namespace App\Http\Controllers;

use App\Image;
use App\Post;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    public function uploadForm()
    {
        return view('article.articleForm');
    }

    public function uploadSubmit(Request $request)
    {
        $this->validate($request, [
            'postText' => 'required',
            'photos' => 'required|max:71680',
            'photos.*' => 'image|mimes:jpeg,jpg,gif,png|max:3072'
        ]);

        $post = Post::create($request->all());

        foreach ($request->photos as $photo) {
            $filename = $photo->store(config('image.path'), 'public');

            if($filename === "")
            {
                $post->delete();

                return back()->with('failed', 'L\'image ne peut pas être ajouter !');
            } else {
                $post->images()->save(
                    new Image([
                        'nameImage'=>$filename
                    ])
                );

                \Intervention\Image\Facades\Image::make($photo->getRealPath())->resize(655,null, function($constraint) {$constraint->aspectRatio();})->save(public_path('/').$filename);
            }

        }

        return back()->with('success', 'Article ajouté avec succès !');
    }
}
