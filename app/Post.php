<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'postText'
    ];

    public function images()
    {
        return $this->hasMany(Image::class, 'postId');
    }
}
